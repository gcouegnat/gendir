#ifndef DIRECTION_H
#define DIRECTION_H

#include "image.h"
#include "direction.h"
#include "filter.h"
#include "utils.h"

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <cmath>
#include <omp.h>

extern "C" {
#include "dsyevj3.h"
}



//float local_helper(const Image<float>& g, const Image<float>& h, const Image<float>& w, int i, int j, int k) {
//    int ksize = w.size(0);
//    int r = ksize/2;
//    float acc = 0.0;
//    float nrm = 0.0;
//    for(int kk=-r; kk<r+1; ++kk) {
//        for(int jj=-r; jj<r+1; ++jj) {
//            for(int ii=-r; ii<r+1; ++ii) {
//                const float weight = w(ii+r,jj+r,jj+r);
//                nrm += weight;
//                acc += weight * g(i+ii, j+jj, k+kk) *  h(i+ii, j+jj, k+kk);
//            }
//        }
//    }
//    acc /= nrm;
//    return acc;
//}

//void compute_directions(const Image<float>& gradx, const Image<float>& grady, const Image<float>& gradz,
//                        Image<float>& dirx, Image<float>& diry, Image<float>& dirz, const Image<float>& filter) {

//    int nx = gradx.size(0);
//    int ny = gradx.size(1);
//    int nz = gradz.size(2);

//    int r = filter.size(0)/2;

//    //std::cout << r << std::endl;
//    std::cout << "Using " << num_threads() << " thread(s)" << std::endl;

//    #pragma omp parallel for
//    for(int k = r; k < nz-r ; ++k) {
//        if (k%10 == 0) { std::cout << "."; std::cout.flush(); }
//        for(int j = r; j < ny-r ; ++j) {
//            for(int i = r; i < nx-r ; ++i) {

//                double A[3][3];
//                double Q[3][3];
//                double w[3];

//                // construct structure tensor
//                A[0][0] = local_helper(gradx, gradx, filter, i, j, k);
//                A[0][1] = local_helper(gradx, grady, filter, i, j, k);
//                A[0][2] = local_helper(gradx, gradz, filter, i, j, k);
//                A[1][0] = A[0][1];
//                A[1][1] = local_helper(grady, grady, filter, i, j, k);
//                A[1][2] = local_helper(grady, gradz, filter, i, j, k);
//                A[2][0] = A[0][2];
//                A[2][1] = A[1][2];
//                A[2][2] = local_helper(gradz, gradz, filter, i, j, k);

//                // diagonalize A
//                dsyevj3(A, Q, w);

//                // eigenvector of the smallest eigenvalue
//                const int imin = argmin(w);
//                const int imax = argmax(w);
//                const double v[3] = { Q[0][imin],  Q[1][imin],  Q[2][imin] };
//                dirx(i,j,k) = static_cast<float>(v[0]);
//                diry(i,j,k) = static_cast<float>(v[1]);
//                dirz(i,j,k) = static_cast<float>(v[2]);

//                //orient(i,j,k) = argmax(v) + 1;
//                //anisotropy(i,j,k) = 1.0 - (std::abs(w[imin])/ std::abs(w[imax]));

//            }
//        }
//    }


//    //orient.dump("orient.raw");
//    //anisotropy.dump("anisotropy.raw");

//    return;
//}

void compute_directions_sep(Image<float>& gradx, Image<float>& grady, Image<float>& gradz,
                            Image<float>& dirx, Image<float>& diry, Image<float>& dirz,
                            /* Image<float>& norx, Image<float>& nory, Image<float>& norz, */
                            const std::vector<float>& filter,
                            Image<float>& ani, Image<float>& nrj) {

    const int blocksize =  128;
    const int r = filter.size()/2;
    
    const int nx = gradx.size(0);
    const int ny = gradx.size(1);
    const int nz = gradz.size(2);
    

    //int mx = nx/blocksize;
    //int my = ny/blocksize;
    //int mz = nz/blocksize;

    //std::cout << mx << std::endl;
    //std::cout << my << std::endl;
    //std::cout << mz << std::endl;

#pragma omp parallel for
    for(int k=0; k<nz; k+=blocksize-2*r) {

#pragma omp critical 
        {
            std::cout << "[" << omp_get_thread_num() << "] --> slice " << k << " to " << k+blocksize << std::endl;
            std::cout.flush();
        }
        
        
        for(int j=0; j<ny; j+=blocksize-2*r) {
            for(int i=0; i<nx; i+=blocksize-2*r) {

                int kstart = k;
                int kend =   std::min(k + blocksize, nz);
                int jstart = j;
                int jend =   std::min(j + blocksize, ny);
                int istart = i;
                int iend =   std::min(i + blocksize, nx);

                // buffers for gradients
                Image<float> gx(blocksize,blocksize,blocksize);
                Image<float> gy(blocksize,blocksize,blocksize);
                Image<float> gz(blocksize,blocksize,blocksize);

                // buffers for inner products
                Image<float> hxx(blocksize,blocksize,blocksize);
                Image<float> hxy(blocksize,blocksize,blocksize);
                Image<float> hxz(blocksize,blocksize,blocksize);
                Image<float> hyy(blocksize,blocksize,blocksize);
                Image<float> hyz(blocksize,blocksize,blocksize);
                Image<float> hzz(blocksize,blocksize,blocksize);

                // buffers for directions
                Image<float> ux(blocksize,blocksize,blocksize);
                Image<float> uy(blocksize,blocksize,blocksize);
                Image<float> uz(blocksize,blocksize,blocksize);
                /* Image<float> vx(blocksize,blocksize,blocksize); */
                /* Image<float> vy(blocksize,blocksize,blocksize); */
                /* Image<float> vz(blocksize,blocksize,blocksize); */

                // buffer for anisotropy
                Image<float> a(blocksize,blocksize,blocksize);
                Image<float> t(blocksize,blocksize,blocksize);

                // init buffers
                for(int kk=0; kk < (kend-kstart); ++kk) {
                    for(int jj=0; jj < (jend-jstart); ++jj) {
                        for(int ii=0; ii < (iend-istart); ++ii) {
                            gx(ii,jj,kk) = gradx(istart+ii, jstart+jj, kstart+kk);
                            gy(ii,jj,kk) = grady(istart+ii, jstart+jj, kstart+kk);
                            gz(ii,jj,kk) = gradz(istart+ii, jstart+jj, kstart+kk);
                        }
                    }
                }

                // compute inner products
                for(unsigned int kk=0; kk < gx.length() ; ++kk) { hxx[kk] = gx[kk] * gx[kk]; }
                for(unsigned int kk=0; kk < gx.length() ; ++kk) { hxy[kk] = gx[kk] * gy[kk]; }
                for(unsigned int kk=0; kk < gx.length() ; ++kk) { hxz[kk] = gx[kk] * gz[kk]; }
                for(unsigned int kk=0; kk < gx.length() ; ++kk) { hyy[kk] = gy[kk] * gy[kk]; }
                for(unsigned int kk=0; kk < gx.length() ; ++kk) { hyz[kk] = gy[kk] * gz[kk]; }
                for(unsigned int kk=0; kk < gx.length() ; ++kk) { hzz[kk] = gz[kk] * gz[kk]; }

                // filter
                convolve_sep_inplace(hxx, filter);
                convolve_sep_inplace(hxy, filter);
                convolve_sep_inplace(hxz, filter);
                convolve_sep_inplace(hyy, filter);
                convolve_sep_inplace(hyz, filter);
                convolve_sep_inplace(hzz, filter);

                // compute direction
                double A[3][3];
                double Q[3][3];
                double w[3];
                for(int kk=r; kk < (kend-kstart-r); ++kk) {
                    for(int jj=r; jj < (jend-jstart-r); ++jj) {
                        for(int ii=r; ii < (iend-istart-r); ++ii) {
                            A[0][0] = hxx(ii,jj,kk);
                            A[0][1] = A[1][0] = hxy(ii,jj,kk);
                            A[0][2] = A[2][0] = hxz(ii,jj,kk);
                            A[1][1] = hyy(ii,jj,kk);
                            A[1][2] = A[2][1] = hyz(ii,jj,kk);
                            A[2][2] = hzz(ii,jj,kk);
                            // diagonalize A
                            dsyevj3(A, Q, w);

                            // eigenvector of the smallest eigenvalue
                            const int imin = argmin(w);
                            const int imax = argmax(w);
                            //const double v[3] = { Q[0][imin],  Q[1][imin],  Q[2][imin] };
                            ux(ii,jj,kk) = Q[0][imin];
                            uy(ii,jj,kk) = Q[1][imin];
                            uz(ii,jj,kk) = Q[2][imin];
                            
                            /* dirx(istart+ii,jstart+jj,kstart+kk) = Q[0][imin]; */
                            /* diry(istart+ii,jstart+jj,kstart+kk) = Q[1][imin]; //uy(ii,jj,kk); */
                            /* dirz(istart+ii,jstart+jj,kstart+kk) = Q[2][imin]; //uz(ii,jj,kk); */
                            
                            /* vx(ii,jj,kk) = Q[0][imax]; */
                            /* vy(ii,jj,kk) = Q[1][imax]; */
                            /* vz(ii,jj,kk) = Q[2][imax]; */
                            a(ii,jj,kk) = (w[imax]-w[imin])/(w[imax]+w[imin]);
                            t(ii,jj,kk) = w[0] + w[1] + w[2];
                            /* if( std::abs(w[imax])>0) a(ii,jj,kk) = 1.0 - std::abs(w[imin])/std::abs(w[imax]); */
                            //a(ii,jj,kk) = std::abs(w[0]) + std::abs(w[1]) + std::abs(w[2]);
                            
                            /* std::cout << "[ " << Q[0][0] << ", " << Q[0][1] << ", " << Q[0][2] << "\n"; */
                            /* std::cout << "  " << Q[1][0] << ", " << Q[1][1] << ", " << Q[1][2] << "\n"; */
                            /* std::cout << "  " << Q[2][0] << ", " << Q[2][1] << ", " << Q[2][2] << " ]\n"; */

                            /* std::cout << w[0] << ", " << w[1] << ", " << w[2] << std::endl; */

                        
                        }
                    }
                }

/*                 // copy buffers to dir */
                for(int kk=r; kk < (kend-kstart-r); ++kk) {
                    for(int jj=r; jj < (jend-jstart-r); ++jj) {
                        for(int ii=r; ii < (iend-istart-r); ++ii) {
                            dirx(istart+ii,jstart+jj,kstart+kk) = ux(ii,jj,kk);
                            diry(istart+ii,jstart+jj,kstart+kk) = uy(ii,jj,kk);
                            dirz(istart+ii,jstart+jj,kstart+kk) = uz(ii,jj,kk);
                            /* norx(istart+ii,jstart+jj,kstart+kk) = vx(ii,jj,kk); */
                            /* nory(istart+ii,jstart+jj,kstart+kk) = vy(ii,jj,kk); */
                            /* norz(istart+ii,jstart+jj,kstart+kk) = vz(ii,jj,kk); */
                            ani(istart+ii,jstart+jj,kstart+kk)  = a(ii,jj,kk);
                            nrj(istart+ii,jstart+jj,kstart+kk)  = t(ii,jj,kk);
                        }
                    }
                }

            }
        }
    }


    return;
}
#endif /* end of include guard: DIRECTION_H */





