#ifndef SEUILLAGE_H
#define SEUILLAGE_H

#include "image.h"

#include <iostream>
#include <string>
#include <cmath>

template<typename T1>
void threshold_hyst(Image<T1>& in, Image<unsigned char>& out, float low, float high) {

    int nx = in.size(0);
    int ny = in.size(1);
    int nz = in.size(2);

    #pragma omp parallel for
    for( int k=0; k<nz; ++k) {
        for( int j=0; j<ny; ++j) {
            for( int i=0; i<nx; ++i) {
                if(in(i,j,k)>low) {
                    if( in(i,j,k) > high ) {
                        out(i,j,k) = 255;
                    } else {
                        out(i,j,k) = 128;
                    }
                }
            }
        }
    }

    bool flag = true;
    int pass = 0;
    while(flag) {
        std::cout << "Pass " << ++pass << std::endl;
        flag = false;
        #pragma omp parallel for
        for( int k=1; k<nz-1; ++k) {
            for( int j=1; j<ny-1; ++j) {
                for( int i=1; i<nx-1; ++i) {
                    if (out(i,j,k) == 128) {
                        if( (out(i-1,j,k) == 0) || (out(i+1,j,k) == 0) ||  (out(i,j-1,k) == 0) || (out(i,j+1,k) == 0)) {
                            out(i,j,k) = 0;
                            flag = true;
                        } else if ( (out(i-1,j,k) == 255) || (out(i+1,j,k) == 255) || (out(i,j-1,k) == 255) || (out(i,j+1,k) == 255)) {
                            out(i,j,k) = 255;
                            flag = true;
                        }
                    }
                }
            }
        }
    }

}




#endif /* end of include guard: SEUILLAGE_H */
