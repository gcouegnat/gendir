#pragma once

#include "image.h"
#include <cmath>
#include <vector>

template<typename T>
Image<T>& box_filter(int ksize) {
    Image<T>* dest = new Image<T>(ksize,ksize,ksize);
    for(int i=0; i<dest->length(); ++i) {
        (*dest)[i] = T(1);
    }
    return *dest;

}

template <typename T>
Image<T>& gaussian_filter(int ksize) {
    const int r = ksize/2;
    T sigma = ((ksize-1)*0.5 - 1)*0.3 + 0.8;

    Image<T>* dest = new Image<T>(ksize,ksize,ksize);
    for(int k=0; k<ksize; ++k) {
        int kk = k - r;
        for(int j=0; j<ksize; ++j) {
            int jj = j - r;
            for(int i=0; i<ksize; ++i) {
                int ii = i - r;
                (*dest)(i,j,k) = std::exp(-0.5*(ii*ii + jj*jj + kk*kk)/(sigma*sigma));
            }
        }
    }
    return *dest;
}

template <typename T>
std::vector<T> gaussian_filter_sep(int ksize) {
    const int r = ksize/2;
    T sigma = ((ksize-1)*0.5 - 1)*0.3 + 0.8;

    //std::cout << "SIGMA: " << sigma << std::endl;

    std::vector<T> dest(ksize);
    for(int k=0; k<ksize; ++k) {
        int kk = k - r;
        dest[k] = std::exp(-0.5*kk*kk/(sigma*sigma));
    }
    //normalize kernel
    T acc = T(0);
    for(int k = 0; k < ksize; ++k)  {
        acc += dest[k];
    }
    for(int k = 0; k < ksize; ++k) {
        dest[k] /= acc;
    }

    return dest;
}

template <typename T>
std::vector<T> box_filter_sep(int ksize) {

    std::vector<T> dest(ksize);
    for(int k=0; k<ksize; ++k) {
        dest[k] = T(1);
    }
    //normalize kernel
    T acc = T(0);
    for(int k = 0; k < ksize; ++k)  {
        acc += dest[k];
    }
    for(int k = 0; k < ksize; ++k) {
        dest[k] /= acc;
    }

    return dest;
}
template<typename T1, typename T2, typename T3>
void convolve(const Image<T1>& src, Image<T2>& dest, const Image<T3>& filter, bool normalize=true) {

    int nx = src.size(0);
    int ny = src.size(1);
    int nz = src.size(2);

    int ksize = filter.size(0);
    int r = ksize/2;

    T3 nrm = T3(1);
    // compute filter norm
    if (normalize) {
        nrm = T3(0);
        for(unsigned int i=0; i<filter.length(); ++i)
            nrm +=  std::abs(filter[i]);
    }

    #pragma omp parallel for
    for(int k = r; k < nz - r; ++k) {
        if (k%10 == 0) { std::cout << "."; std::cout.flush(); }
        for(int j = r; j < ny - r; ++j) {
            for(int i = r; i < nx - r; ++i) {
                T2 acc = T2(0);
                for (int w = 0; w < ksize; ++w)
                    for (int v = 0; v < ksize; ++v)
                        for (int u = 0; u < ksize; ++u)
                            acc += filter(u,v,w) * src(i+u-r, j+v-r, k+w-r);
                dest(i,j,k) = static_cast<T2>(acc/nrm);
            }
        }
    }
    std::cout << std::endl;
}


template<typename T1, typename T2>
void convolve_sep_inplace(Image<T1>& src, const std::vector<T2>& kernel) {

    const int nx = src.size(0);
    const int ny = src.size(1);
    const int nz = src.size(2);

    const int ksize = kernel.size();
    const int rsize = ksize/2;

    // first pass
    //std::cout << "First pass" << std::endl;
    for(int k=0; k<nz; ++k) {
        for(int j=0; j<ny; ++j) {
            // copy row to buffer
            std::vector<T1> bufin(nx);
            std::vector<T2> bufout(nx, T2(0));
            for(int u=0; u<nx; ++u) bufin[u] = src(u,j,k);
            // convolve kernel
            for(int u=rsize; u<nx-rsize; ++u) {
                for(int v=-rsize; v<rsize+1; ++v)
                    bufout[u] += kernel[rsize + v] * bufin[u + v];
            }
            // copy buffer to row
            for(int u=0; u<nx; ++u) src(u,j,k) = static_cast<T1>(bufout[u]);
        }
    }

    // second pass
    //std::cout << "First pass" << std::endl;
    for(int k=0; k<nz; ++k) {
        for(int i=0; i<nx; ++i) {
            // copy row to buffer
            std::vector<T1> bufin(ny);
            std::vector<T2> bufout(ny, T2(0));
            for(int u=0; u<ny; ++u) bufin[u] = src(i,u,k);
            // convolve kernel
            for(int u=rsize; u<ny-rsize; ++u) {
                for(int v=-rsize; v<rsize+1; ++v)
                    bufout[u] += kernel[rsize + v] * bufin[u + v];
            }
            // copy buffer to row
            for(int u=0; u<ny; ++u) src(i,u,k) = static_cast<T1>( bufout[u] );
        }
    }

    // third pass
    //std::cout << "First pass" << std::endl;
    for(int j=0; j<ny; ++j) {
        for(int i=0; i<nx; ++i) {
            // copy row to buffer
            std::vector<T1> bufin(nz);
            std::vector<T2> bufout(nz, T2(0));
            for(int u=0; u<nz; ++u) bufin[u] = src(i,j,u);
            // convolve kernel
            for(int u=rsize; u<nz-rsize; ++u) {
                for(int v=-rsize; v<rsize+1; ++v)
                    bufout[u] += kernel[rsize + v] * bufin[u + v];
            }
            // copy buffer to row
            for(int u=0; u<nz; ++u) src(i,j,u) = static_cast<T1> (bufout[u]);
        }
    }

}



