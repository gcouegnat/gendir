#ifndef UTILS_H
#define UTILS_H

#include <meta.h>

#include <string>
#include <sstream>
#include <regex>

#include <cmath>

using std::string;

string basename(const string& src, bool remove_extenstion=true) {
    string s = src;
    size_t b = s.find_last_of("/\\");
    s = s.substr(b+1);
    if(remove_extenstion) {
        size_t e = s.find_last_of(".");
        s = s.substr(0,e);
    }
    return s;
}

bool guess_size_from_filename(const string& filename, int& nx, int& ny, int& nz)
{
    std::regex r("([0-9]+)x([0-9]+)x([0-9]+)");
    std::smatch m;

    std::regex_search(filename, m , r);
    if (m.size()>0) {
        nx = meta::lexical_cast<int>(m[1]);
        ny = meta::lexical_cast<int>(m[2]);
        nz = meta::lexical_cast<int>(m[3]);
    }
    return (m.size()>0);
}

void strip_filename(const string& filename, string& prefix, string& suffix) {

    prefix = "";
    suffix = filename;

    std::regex r("([0-9]+)x([0-9]+)x([0-9]+)");
    std::smatch m;
    std::regex_search(filename, m , r);
    if (m.size()>0) {
        prefix = m.prefix();
        suffix = m.suffix();
    }

    return;
}


template<typename T>
int argmin(T x[3]) {
    int i = 0;
    if (std::abs(x[1]) < std::abs(x[i])) {
        i = 1;
    }
    if (std::abs(x[2]) < std::abs(x[i])) {
        i = 2;
    }
    return i;
}

template<typename T>
int argmax(T x[3]) {
    int i = 0;
    if (std::abs(x[1]) > std::abs(x[i])) {
        i = 1;
    }
    if (std::abs(x[2]) > std::abs(x[i])) {
        i = 2;
    }
    return i;
}

template <typename T>
void cross(const T u[3], const T v[3], T w[3]) {
    w[0] = u[1]*v[2] - u[2]*v[1];
    w[1] = u[2]*v[0] - u[0]*v[2];
    w[2] = u[0]*v[1] - u[1]*v[0];

    return;
}


#ifdef _OPENMP
#include <omp.h>
#endif

unsigned int num_threads() {
    unsigned int nthreads = 1;
#ifdef _OPENMP
#pragma omp parallel
    {
#pragma omp master
        {
            nthreads = omp_get_num_threads();
        }
    }
#endif    

    return nthreads;
}




#endif /* end of include guard: UTILS_H */
