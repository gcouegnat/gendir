#include <argparser.h>
#include <image.h>
#include <utils.h>

#include <cmath>

float FloatSwap(float f) {
  union {
    float f;
    unsigned char b[4];
  } dat1, dat2;

  dat1.f = f;
  dat2.b[0] = dat1.b[3];
  dat2.b[1] = dat1.b[2];
  dat2.b[2] = dat1.b[1];
  dat2.b[3] = dat1.b[0];
  return dat2.f;
}

int main(int argc, const char *argv[]) {
  Argparser parser;
  parser.add_option<std::string>("basename", "");
  parser.add_option<int>("nx", "Dimension X", -1);
  parser.add_option<int>("ny", "Dimension Y", -1);
  parser.add_option<int>("nz", "Dimension Z", -1);
  parser.parse(argc, argv);

  const std::string basename = parser.get<std::string>("basename");

  int nx, ny, nz;
  if (!guess_size_from_filename(basename, nx, ny, nz)) {
    nx = parser.get<int>("nx");
    ny = parser.get<int>("ny");
    nz = parser.get<int>("nz");
  }
  std::cout << nx << ", " << ny << ", " << nz << std::endl;

  Image<unsigned char> data(nx, ny, nz);
  Image<float> dirx(nx, ny, nz);
  Image<float> diry(nx, ny, nz);
  Image<float> dirz(nx, ny, nz);
  /* Image<float> norx(nx,ny,nz); */
  /* Image<float> nory(nx,ny,nz); */
  /* Image<float> norz(nx,ny,nz); */
  Image<float> ani(nx, ny, nz);
  Image<unsigned char> ori(nx, ny, nz);

  data.load(basename + ".raw");
  dirx.load(basename + "_dirx.raw");
  diry.load(basename + "_diry.raw");
  dirz.load(basename + "_dirz.raw");
  /* norx.load(basename + "_norx.raw"); */
  /* nory.load(basename + "_nory.raw"); */
  /* norz.load(basename + "_norz.raw"); */
  /* ani.load(basename + "_ani.raw"); */
  ori.load(basename + "_ori.raw");

  std::string outfile(basename + ".vtk");
  std::ofstream output;
  output.open(outfile.c_str());
  std::cout << "Writing " << outfile << std::endl;

  output << "# vtk DataFile Version 2.0\n";
  output << "header\n";
  output << "BINARY\n";
  output << "DATASET STRUCTURED_POINTS\n";
  output << "DIMENSIONS " << nx << " " << ny << " " << nz << '\n';
  output << "ORIGIN 0 0 0\n";
  output << "SPACING 1 1 1\n";

  output << "POINT_DATA " << nx * ny * nz << '\n';
  output << "SCALARS ndg unsigned_char 1\n";
  output << "LOOKUP_TABLE default\n";
  output.write((char *)&data[0], nx * ny * nz * sizeof(unsigned char));

  output << "SCALARS ori unsigned_char 1\n";
  output << "LOOKUP_TABLE default\n";
  output.write((char *)&ori[0], nx * ny * nz * sizeof(unsigned char));

  output << "SCALARS dir float 3\n";
  output << "LOOKUP_TABLE default\n";
  for (int i = 0; i < nx * ny * nz; ++i) {
    float u = FloatSwap(dirx[i]);
    float v = FloatSwap(diry[i]);
    float w = FloatSwap(dirz[i]);

    output.write((char *)&u, sizeof(float));
    output.write((char *)&v, sizeof(float));
    output.write((char *)&w, sizeof(float));
  }

  /* output << "SCALARS nor float 3\n"; */
  /* output << "LOOKUP_TABLE default\n"; */
  /* for(int i=0; i<nx*ny*nz; ++i) { */
  /*     float u =  FloatSwap(norx[i]); */
  /*     float v =  FloatSwap(nory[i]); */
  /*     float w =  FloatSwap(norz[i]); */

  /*     output.write((char*) &u, sizeof(float)); */
  /*     output.write((char*) &v, sizeof(float)); */
  /*     output.write((char*) &w, sizeof(float)); */
  /* } */

  /* output << "SCALARS anisotropy float 1\n"; */
  /* output << "LOOKUP_TABLE default\n"; */
  /* for(int i=0; i<nx*ny*nz; ++i) { */
  /*     float u =  FloatSwap(ani[i]); */
  /*     output.write((char*) &u, sizeof(float)); */
  /* } */

  output.close();
  return 0;
}
