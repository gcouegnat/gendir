#include <argparser.h>
#include <image.h>

#include <cmath>

int main(int argc, const char *argv[]) {
  Argparser parser;
  parser.add_option<std::string>("basename", "");
  parser.add_option<int>("nx", "Dimension X");
  parser.add_option<int>("ny", "Dimension Y");
  parser.add_option<int>("nz", "Dimension Z");
  parser.parse(argc, argv);

  const int nx = parser.get<int>("nx");
  const int ny = parser.get<int>("ny");
  const int nz = parser.get<int>("nz");
  std::cout << nx << ", " << ny << ", " << nz << std::endl;

  Image<float> dirx(nx, ny, nz);
  Image<float> diry(nx, ny, nz);
  Image<float> dirz(nx, ny, nz);
  const std::string basename = parser.get<std::string>("basename");

  dirx.load(basename + "_dirx.raw");
  diry.load(basename + "_diry.raw");
  dirz.load(basename + "_dirz.raw");

  Image<unsigned char> ori(nx, ny, nz);
  for (size_t k = 0; k < ori.length(); ++k) {
    float nrm =
        std::sqrt(dirx[k] * dirx[k] + diry[k] * diry[k] + dirz[k] * dirz[k]);
    if (nrm > 0.1) {
      if ((std::abs(dirx[k]) > std::abs(diry[k])) &&
          (std::abs(dirx[k]) > std::abs(dirz[k]))) {
        ori[k] = 1;
      } else if ((std::abs(diry[k]) > std::abs(dirz[k])) &&
                 (std::abs(diry[k]) > std::abs(dirx[k]))) {
        ori[k] = 2;
      } else {
        ori[k] = 3;
      }
    } else {
      ori[k] = 0;
    }
  }

  ori.dump(basename + "_ori.raw");

  return 0;
}
