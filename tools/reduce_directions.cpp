#include <argparser.h>
#include <image.h>
#include <utils.h>

#include <cmath>

int main(int argc, const char *argv[]) {
  Argparser parser;
  parser.add_option<std::string>("basename", "");
  parser.add_option<int>("nx", "", -1);
  parser.add_option<int>("ny", "", -1);
  parser.add_option<int>("nz", "", -1);
  parser.add_option<int>("factor", "", 2);
  parser.parse(argc, argv);

  const std::string basename = parser.get<std::string>("basename");
  int nx, ny, nz;
  if (!guess_size_from_filename(basename, nx, ny, nz)) {
    nx = parser.get<int>("nx");
    ny = parser.get<int>("ny");
    nz = parser.get<int>("nz");
  }
  const int factor = parser.get<int>("factor");

  const int mx = nx / factor;
  const int my = ny / factor;
  const int mz = nz / factor;

  std::cout << nx << ", " << ny << ", " << nz << std::endl;
  std::cout << mx << ", " << my << ", " << mz << std::endl;

  Image<float> dirx(nx, ny, nz);
  Image<float> diry(nx, ny, nz);
  Image<float> dirz(nx, ny, nz);

  dirx.load(basename + "_dirx.raw");
  diry.load(basename + "_diry.raw");
  dirz.load(basename + "_dirz.raw");

  Image<float> outx(mx, my, mz);
  Image<float> outy(mx, my, mz);
  Image<float> outz(mx, my, mz);

  for (int k = 0; k < mz; ++k) {
    std::cout << k << std::endl;
    for (int j = 0; j < my; ++j) {
      for (int i = 0; i < mx; ++i) {

        float ux = 0;
        float uy = 0;
        float uz = 0;

        for (int w = k * factor; w < std::min((k + 1) * factor, nz); ++w) {
          for (int v = j * factor; v < std::min((j + 1) * factor, ny); ++v) {
            for (int u = i * factor; u < std::min((i + 1) * factor, nx); ++u) {
              ux += dirx(u, v, w);
              uy += diry(u, v, w);
              uz += dirz(u, v, w);
            }
          }
        }

        // normalize the vector
        float nrm = std::sqrt(ux * ux + uy * uy + uz * uz);
        outx(i, j, k) = ux / nrm;
        outy(i, j, k) = uy / nrm;
        outz(i, j, k) = uz / nrm;
      }
    }
  }

  std::string prefix, suffix;
  strip_filename(basename, prefix, suffix);

  std::ostringstream ossx, ossy, ossz;
  ossx << prefix << mx << "x" << my << "x" << mz << suffix << "_dirx.raw";
  ossy << prefix << mx << "x" << my << "x" << mz << suffix << "_diry.raw";
  ossz << prefix << mx << "x" << my << "x" << mz << suffix << "_dirz.raw";

  outx.dump(ossx.str());
  outy.dump(ossy.str());
  outz.dump(ossz.str());

  return 0;
}
