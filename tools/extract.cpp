#include <argparser.h>
#include <cmath>
#include <image.h>
#include <utils.h>

int main(int argc, const char *argv[]) {
  Argparser parser;
  parser.add_option<std::string>("input", "");
  parser.add_option<int>("nx", "", -1);
  parser.add_option<int>("ny", "", -1);
  parser.add_option<int>("nz", "", -1);
  parser.add_option<int>("sx", "starting point x");
  parser.add_option<int>("sy", "");
  parser.add_option<int>("sz", "");
  parser.add_option<int>("tx", "ending point x");
  parser.add_option<int>("ty", "");
  parser.add_option<int>("tz", "");
  parser.parse(argc, argv);

  const std::string filename = parser.get<std::string>("input");

  int nx, ny, nz;
  if (!guess_size_from_filename(filename, nx, ny, nz)) {
    nx = parser.get<int>("nx");
    ny = parser.get<int>("ny");
    nz = parser.get<int>("nz");
  }

  const int sx = parser.get<int>("sx");
  const int sy = parser.get<int>("sy");
  const int sz = parser.get<int>("sz");
  const int tx = parser.get<int>("tx");
  const int ty = parser.get<int>("ty");
  const int tz = parser.get<int>("tz");

  const int mx = tx - sx;
  const int my = ty - sy;
  const int mz = tz - sz;

  std::cout << nx << ", " << ny << ", " << nz << std::endl;
  std::cout << mx << ", " << my << ", " << mz << std::endl;

  Image<unsigned char> src(nx, ny, nz);
  src.load(filename);

  Image<unsigned char> dest(mx, my, mz);

#pragma omp parallel for
  for (int k = 0; k < mz; ++k) {
    for (int j = 0; j < my; ++j) {
      for (int i = 0; i < mx; ++i) {
        dest(i, j, k) = src(i + sx, j + sy, k + sz);
      }
    }
  }

  std::string prefix, suffix;
  strip_filename(basename(filename, false), prefix, suffix);
  std::ostringstream oss;
  oss << prefix << mx << "x" << my << "x" << mz << suffix;
  dest.dump(oss.str());

  return 0;
}
