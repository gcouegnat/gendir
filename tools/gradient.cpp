#include <argparser.h>
#include <filter.h>
#include <g3d10.h>
#include <image.h>
#include <utils.h>

#include <cstdlib>
#include <omp.h>

int main(int argc, const char *argv[]) {
  Argparser parser;
  parser.add_option<std::string>("input", "Input filename");
  parser.add_option<int>("nx", "Size in X direction", -1);
  parser.add_option<int>("ny", "Size in Y direction", -1);
  parser.add_option<int>("nz", "Size in Z direction", -1);
  parser.parse(argc, argv);

  const std::string filename = parser.get<std::string>("input");

  int nx, ny, nz;
  if (!guess_size_from_filename(filename, nx, ny, nz)) {
    nx = parser.get<int>("nx");
    ny = parser.get<int>("ny");
    nz = parser.get<int>("nz");
  }

  std::cout << "Using " << num_threads() << " thread(s)" << std::endl;

  Image<unsigned char> src(nx, ny, nz);
  src.load(filename);

  Image<float> dest(nx, ny, nz);
  Image<float> filter;

  std::cout << "Computing gradient X" << std::endl;
  filter = g3d10::gradx();
  convolve(src, dest, filter);
  dest.dump(basename(filename) + "_gradx.raw");

  std::cout << "Computing gradient Y" << std::endl;
  filter = g3d10::grady();
  convolve(src, dest, filter);
  dest.dump(basename(filename) + "_grady.raw");

  std::cout << "Computing gradient Z" << std::endl;
  filter = g3d10::gradz();
  convolve(src, dest, filter);
  dest.dump(basename(filename) + "_gradz.raw");

  return 0;
}
