#include <argparser.h>
#include <cmath>
#include <image.h>
#include <utils.h>

int main(int argc, const char *argv[]) {
  Argparser parser;
  parser.add_option<std::string>("basename", "");
  parser.add_option<int>("nx", "", -1);
  parser.add_option<int>("ny", "", -1);
  parser.add_option<int>("nz", "", -1);
  parser.add_option<int>("sx", "starting point x");
  parser.add_option<int>("sy", "");
  parser.add_option<int>("sz", "");
  parser.add_option<int>("tx", "ending point x");
  parser.add_option<int>("ty", "");
  parser.add_option<int>("tz", "");
  parser.parse(argc, argv);

  const std::string basename = parser.get<std::string>("basename");

  int nx, ny, nz;
  if (!guess_size_from_filename(basename, nx, ny, nz)) {
    nx = parser.get<int>("nx");
    ny = parser.get<int>("ny");
    nz = parser.get<int>("nz");
  }

  const int sx = parser.get<int>("sx");
  const int sy = parser.get<int>("sy");
  const int sz = parser.get<int>("sz");
  const int tx = parser.get<int>("tx");
  const int ty = parser.get<int>("ty");
  const int tz = parser.get<int>("tz");

  const int mx = tx - sx;
  const int my = ty - sy;
  const int mz = tz - sz;

  std::cout << nx << ", " << ny << ", " << nz << std::endl;
  std::cout << mx << ", " << my << ", " << mz << std::endl;

  Image<float> dirx(nx, ny, nz);
  Image<float> diry(nx, ny, nz);
  Image<float> dirz(nx, ny, nz);

  dirx.load(basename + "_dirx.raw");
  diry.load(basename + "_diry.raw");
  dirz.load(basename + "_dirz.raw");

  Image<float> outx(mx, my, mz);
  Image<float> outy(mx, my, mz);
  Image<float> outz(mx, my, mz);

#pragma omp parallel for
  for (int k = 0; k < mz; ++k) {
    for (int j = 0; j < my; ++j) {
      for (int i = 0; i < mx; ++i) {
        outx(i, j, k) = dirx(i + sx, j + sy, k + sz);
      }
    }
  }

#pragma omp parallel for
  for (int k = 0; k < mz; ++k) {
    for (int j = 0; j < my; ++j) {
      for (int i = 0; i < mx; ++i) {
        outy(i, j, k) = diry(i + sx, j + sy, k + sz);
      }
    }
  }

#pragma omp parallel for
  for (int k = 0; k < mz; ++k) {
    for (int j = 0; j < my; ++j) {
      for (int i = 0; i < mx; ++i) {
        outz(i, j, k) = dirz(i + sx, j + sy, k + sz);
      }
    }
  }

  std::string prefix, suffix;
  strip_filename(basename, prefix, suffix);

  std::ostringstream ossx, ossy, ossz;
  ossx << prefix << mx << "x" << my << "x" << mz << suffix << "_dirx.raw";
  ossy << prefix << mx << "x" << my << "x" << mz << suffix << "_diry.raw";
  ossz << prefix << mx << "x" << my << "x" << mz << suffix << "_dirz.raw";

  outx.dump(ossx.str());
  outy.dump(ossy.str());
  outz.dump(ossz.str());

  return 0;
}
