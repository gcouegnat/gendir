#include <argparser.h>
#include <image.h>
#include <utils.h>

#include <cmath>

int helper_index(float x, int n);
static int cell_index(float u, float v, float w, int n);
static int argmax(float x, float y, float z);

int main(int argc, const char *argv[]) {
  Argparser parser;
  parser.add_option<std::string>("basename", "Basename");
  parser.add_option<int>("ndiv", "Number of division in the sphere mesh");
  parser.add_option<int>("threshold", "Threshold", 0);

  parser.add_option<int>("nx", "Dimension X", -1);
  parser.add_option<int>("ny", "Dimension Y", -1);
  parser.add_option<int>("nz", "Dimension Z", -1);
  parser.parse(argc, argv);

  const std::string basename = parser.get<std::string>("basename");
  int nx, ny, nz;
  if (!guess_size_from_filename(basename, nx, ny, nz)) {
    nx = parser.get<int>("nx");
    ny = parser.get<int>("ny");
    nz = parser.get<int>("nz");
  }
  std::cout << nx << ", " << ny << ", " << nz << std::endl;

  Image<unsigned char> data(nx, ny, nz);
  Image<float> dirx(nx, ny, nz);
  Image<float> diry(nx, ny, nz);
  Image<float> dirz(nx, ny, nz);

  data.load(basename + ".raw");
  dirx.load(basename + "_dirx.raw");
  diry.load(basename + "_diry.raw");
  dirz.load(basename + "_dirz.raw");

  int ndiv = parser.get<int>("ndiv");
  int threshold = parser.get<int>("threshold");

  std::vector<float> hist(6 * ndiv * ndiv, 0);

  for (size_t k = 0; k < data.length(); ++k) {

    unsigned char ndg = data[k];
    float ux = dirx[k];
    float uy = diry[k];
    float uz = dirz[k];

    float nrm = ux * ux + uy * uy + uz * uz;

    if ((ndg >= threshold) && (nrm > 0.9)) {
      int idx = cell_index(ux, uy, uz, ndiv);
      hist[idx]++;
    }
  }

  std::ofstream output;
  output.open("hist.dat");

  for (size_t i = 0; i < hist.size(); ++i) {
    output << hist[i] << '\n';
  }

  output.close();

  return 0;
}

inline int helper_index(float x, int n) {
  return static_cast<int>(
      std::floor(0.5f * (4.0f * n / M_PI * std::atan(x) + n)));
}

int cell_index(float ux, float uy, float uz, int n) {

  int imax = argmax(std::abs(ux), std::abs(uy), std::abs(uz));

  float u[3], w[3];

  u[0] = ux;
  u[1] = uy;
  u[2] = uz;

  w[0] = u[0] / u[imax];
  w[1] = u[1] / u[imax];
  w[2] = u[2] / u[imax];

  int index;
  int idx1, idx2, idx3;

  switch (imax) {
  case 0: {
    idx1 = 0;
    idx2 = helper_index(w[1], n);
    idx3 = helper_index(w[2], n);
  } break;

  case 1: {
    idx1 = 1;
    idx2 = helper_index(w[0], n);
    idx3 = helper_index(w[2], n);
  } break;

  case 2: {
    idx1 = 2;
    idx2 = helper_index(w[0], n);
    idx3 = helper_index(w[1], n);
  } break;
  default:
    index = -1;
  }

  index = idx1 * n * n + idx2 * n + idx3;

  return index;
}

inline int argmax(float x, float y, float z) {

  int imax = 0;

  if (x > y) {
    if (x > z) {
      imax = 0; // x>y && x>z
    } else {
      imax = 2; // x>y && x<z
    }
  } else {
    if (y > z) { // x<y && y>z
      imax = 1;
    } else {
      imax = 2;
    }
  }

  return imax;
}
