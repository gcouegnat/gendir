#include <argparser.h>
#include <image.h>
#include <utils.h>

#include <iostream>
#include <vector>
#include <cstdlib>

struct Cell {
  int nodes[8];
  unsigned char value;
};
struct Vertex {
  float coords[3];
};

int main(int argc, const char *argv[]) {
  Argparser parser;
  parser.add_option<std::string>("input", "Input filename");
  parser.add_option<int>("nx", "Dimension X", -1);
  parser.add_option<int>("ny", "Dimension Y", -1);
  parser.add_option<int>("nz", "Dimension Z", -1);
  parser.add_option<int>("threshold",
                         "Remove voxels with value less than threshold", 0);
  parser.add_option<std::string>("format", "Mesh format: vtk|inp|mesh", "vtk");
  parser.add_option<float>("scale", "Mesh scale factor", 1);
  parser.parse(argc, argv);

  const std::string filename = parser.get<std::string>("input");
  const unsigned char threshold =
      static_cast<unsigned char>(parser.get<int>("threshold"));
  const float scale = parser.get<float>("scale");

  int nx, ny, nz;
  if (!guess_size_from_filename(filename, nx, ny, nz)) {
    nx = parser.get<int>("nx");
    ny = parser.get<int>("ny");
    nz = parser.get<int>("nz");
  }

  std::cout << nx << ", " << ny << ", " << nz << std::endl;
  std::cout << "Scale: " << scale << std::endl;

  Image<unsigned char> I(nx, ny, nz);
  I.load(filename);

  Image<float> dirx(nx, ny, nz);
  Image<float> diry(nx, ny, nz);
  Image<float> dirz(nx, ny, nz);

  dirx.load(basename(filename) + "_dirx.raw");
  diry.load(basename(filename) + "_diry.raw");
  dirz.load(basename(filename) + "_dirz.raw");

  // init vertices coordinates
  int nvert = (nx + 1) * (ny + 1) * (nz + 1);
  std::vector<Vertex> vertices(nvert);
  for (int k = 0; k < nz + 1; ++k) {
    for (int j = 0; j < ny + 1; ++j) {
      for (int i = 0; i < nx + 1; ++i) {
        int c = i + (nx + 1) * j + (nx + 1) * (ny + 1) * k;
        vertices[c].coords[0] = i;
        vertices[c].coords[1] = j;
        vertices[c].coords[2] = k;
      }
    }
  }

  // init cells connectivity
  int ncell = nx * ny * nz;
  std::vector<Cell> cells;
  cells.reserve(ncell);

  int nc = 0;
  for (int k = 0; k < nz; ++k) {
    for (int j = 0; j < ny; ++j) {
      for (int i = 0; i < nx; ++i) {
        if (I(i, j, k) >= threshold) {
          cells[nc].nodes[0] = i + (nx + 1) * j + (nx + 1) * (ny + 1) * k;
          cells[nc].nodes[1] = (i + 1) + (nx + 1) * j + (nx + 1) * (ny + 1) * k;
          cells[nc].nodes[2] =
              (i + 1) + (nx + 1) * (j + 1) + (nx + 1) * (ny + 1) * k;
          cells[nc].nodes[3] = i + (nx + 1) * (j + 1) + (nx + 1) * (ny + 1) * k;
          cells[nc].nodes[4] = i + (nx + 1) * j + (nx + 1) * (ny + 1) * (k + 1);
          cells[nc].nodes[5] =
              (i + 1) + (nx + 1) * j + (nx + 1) * (ny + 1) * (k + 1);
          cells[nc].nodes[6] =
              (i + 1) + (nx + 1) * (j + 1) + (nx + 1) * (ny + 1) * (k + 1);
          cells[nc].nodes[7] =
              i + (nx + 1) * (j + 1) + (nx + 1) * (ny + 1) * (k + 1);
          cells[nc].value = I(i, j, k);
          nc++;
        }
      }
    }
  }

  std::vector<int> renum(nvert, -1);
  std::vector<Vertex *> filtered_vertices;
  filtered_vertices.reserve(nvert);

  // mark the vertex belonging to a cell
  for (int i = 0; i < nc; ++i) {
    for (int j = 0; j < 8; ++j) {
      renum[cells[i].nodes[j]] = 1;
    }
  }

  // renumber vertices
  int nv = 0;
  for (int i = 0; i < nvert; ++i) {
    if (renum[i] > 0) {
      filtered_vertices.push_back(&vertices[i]);
      renum[i] = nv++;
    }
  }

  // renumber cells
  for (int i = 0; i < nc; ++i) {
    cells[i].nodes[0] = renum[cells[i].nodes[0]];
    cells[i].nodes[1] = renum[cells[i].nodes[1]];
    cells[i].nodes[2] = renum[cells[i].nodes[2]];
    cells[i].nodes[3] = renum[cells[i].nodes[3]];
    cells[i].nodes[4] = renum[cells[i].nodes[4]];
    cells[i].nodes[5] = renum[cells[i].nodes[5]];
    cells[i].nodes[6] = renum[cells[i].nodes[6]];
    cells[i].nodes[7] = renum[cells[i].nodes[7]];
  }

  std::cout << "Output mesh have " << nv << " vertices and " << nc
            << " cells\n";

  // Writing ouptut

  std::string ext = parser.get<std::string>("format");

  std::string outfile = basename(filename) + "." + ext;
  std::ofstream output;
  output.open(outfile.c_str());
  std::cout << "Writing " << outfile << std::endl;

  if (ext == "vtk") {
    // ------
    // VTK
    // ------
    output << "# vtk DataFile Version 2.0\n";
    output << "header\n";
    output << "ASCII\n";
    output << "DATASET UNSTRUCTURED_GRID\n";
    output << "POINTS " << nv << " float\n";
    for (int i = 0; i < nv; ++i) {
      output << scale * filtered_vertices[i]->coords[0] << " "
             << scale * filtered_vertices[i]->coords[1] << " "
             << scale * filtered_vertices[i]->coords[2] << '\n';
    }

    output << "CELLS " << nc << " " << 9 * nc << '\n';
    for (int i = 0; i < nc; ++i) {
      output << "8 " << cells[i].nodes[0] << " " << cells[i].nodes[1] << " "
             << cells[i].nodes[2] << " " << cells[i].nodes[3] << " "
             << cells[i].nodes[4] << " " << cells[i].nodes[5] << " "
             << cells[i].nodes[6] << " " << cells[i].nodes[7] << '\n';
    }

    output << "CELL_TYPES " << nc << '\n';
    for (int i = 0; i < nc; ++i)
      output << "12\n";

    output << "CELL_DATA " << nc << '\n';
    output << "SCALARS ndg unsigned_char 1\n";
    output << "LOOKUP_TABLE default\n";
    for (int i = 0; i < nc; ++i) {
      // output << static_cast<int>(cells[i].value) << '\n';
      int ii =
          static_cast<int>(filtered_vertices[cells[i].nodes[0]]->coords[0]);
      int jj =
          static_cast<int>(filtered_vertices[cells[i].nodes[0]]->coords[1]);
      int kk =
          static_cast<int>(filtered_vertices[cells[i].nodes[0]]->coords[2]);
      output << static_cast<int>(I(ii, jj, kk)) << '\n';
    }

    output << "scalars dir float 3\n";
    output << "lookup_table default\n";
    for (int i = 0; i < nc; ++i) {
      // output << static_cast<int>(cells[i].value) << '\n';
      int ii =
          static_cast<int>(filtered_vertices[cells[i].nodes[0]]->coords[0]);
      int jj =
          static_cast<int>(filtered_vertices[cells[i].nodes[0]]->coords[1]);
      int kk =
          static_cast<int>(filtered_vertices[cells[i].nodes[0]]->coords[2]);
      output << static_cast<float>(dirx(ii, jj, kk)) << " "
             << static_cast<float>(diry(ii, jj, kk)) << " "
             << static_cast<float>(dirz(ii, jj, kk)) << '\n';
    }

  } else if (ext == "inp") {

    // ------
    // Abaqus
    // ------

    // Nodes
    output << "*node, nset=all_node\n";
    for (int i = 0; i < nv; ++i) {
      output << i + 1 << ", " << filtered_vertices[i]->coords[0] << ", "
             << filtered_vertices[i]->coords[1] << ", "
             << filtered_vertices[i]->coords[2] << "\n";
    }

    // Elements
    output << "*element, type=c3d8, elset=all_element\n";
    for (int i = 0; i < nc; ++i) {
      output << i + 1 << ", " << cells[i].nodes[0] + 1 << ", "
             << cells[i].nodes[1] + 1 << ", " << cells[i].nodes[2] + 1 << ", "
             << cells[i].nodes[3] + 1 << ", " << cells[i].nodes[4] + 1 << ", "
             << cells[i].nodes[5] + 1 << ", " << cells[i].nodes[6] + 1 << ", "
             << cells[i].nodes[7] + 1 << '\n';
    }

    // Nsets
    float xmax = 0.0;
    float ymax = 0.0;
    float zmax = 0.0;
    float xmin = nx;
    float ymin = ny;
    float zmin = nz;

    for (int i = 0; i < nv; ++i) {
      xmin = std::min(xmin, filtered_vertices[i]->coords[0]);
      ymin = std::min(ymin, filtered_vertices[i]->coords[1]);
      zmin = std::min(zmin, filtered_vertices[i]->coords[2]);
      xmax = std::max(xmax, filtered_vertices[i]->coords[0]);
      ymax = std::max(ymax, filtered_vertices[i]->coords[1]);
      zmax = std::max(zmax, filtered_vertices[i]->coords[2]);
    }

    std::cout << xmin << ", " << ymin << ", " << zmin << std::endl;
    std::cout << xmax << ", " << ymax << ", " << zmax << std::endl;

    std::vector<int> nset_xmin, nset_ymin, nset_zmin;
    std::vector<int> nset_xmax, nset_ymax, nset_zmax;

    nset_xmin.reserve(nv);
    nset_ymin.reserve(nv);
    nset_zmin.reserve(nv);
    nset_xmax.reserve(nv);
    nset_ymax.reserve(nv);
    nset_zmax.reserve(nv);

    for (int i = 0; i < nv; ++i) {
      if (std::abs(filtered_vertices[i]->coords[0] - xmin) < 1.0e-6) {
        nset_xmin.push_back(i);
      }
      if (std::abs(filtered_vertices[i]->coords[0] - xmax) < 1.0e-6) {
        nset_xmax.push_back(i);
      }
      if (std::abs(filtered_vertices[i]->coords[1] - ymin) < 1.0e-6) {
        nset_ymin.push_back(i);
      }
      if (std::abs(filtered_vertices[i]->coords[1] - ymax) < 1.0e-6) {
        nset_ymax.push_back(i);
      }
      if (std::abs(filtered_vertices[i]->coords[2] - zmin) < 1.0e-6) {
        nset_zmin.push_back(i);
      }
      if (std::abs(filtered_vertices[i]->coords[2] - zmax) < 1.0e-6) {
        nset_zmax.push_back(i);
      }
    }

    output << "*nset, nset=xmin\n";
    for (int i = 0; i < nset_xmin.size(); ++i) {
      output << nset_xmin[i] + 1 << ", ";
      if ((i + 1) % 8 == 0)
        output << "\n";
    }
    output << "\n";

    output << "*nset, nset=xmax\n";
    for (int i = 0; i < nset_xmax.size(); ++i) {
      output << nset_xmax[i] + 1 << ", ";
      if ((i + 1) % 8 == 0)
        output << "\n";
    }
    output << "\n";

    output << "*nset, nset=ymin\n";
    for (int i = 0; i < nset_ymin.size(); ++i) {
      output << nset_ymin[i] + 1 << ", ";
      if ((i + 1) % 8 == 0)
        output << "\n";
    }
    output << "\n";

    output << "*nset, nset=ymax\n";
    for (int i = 0; i < nset_ymax.size(); ++i) {
      output << nset_ymax[i] + 1 << ", ";
      if ((i + 1) % 8 == 0)
        output << "\n";
    }
    output << "\n";

    output << "*nset, nset=zmin\n";
    for (int i = 0; i < nset_zmin.size(); ++i) {
      output << nset_zmin[i] + 1 << ", ";
      if ((i + 1) % 8 == 0)
        output << "\n";
    }
    output << "\n";

    output << "*nset, nset=zmax\n";
    for (int i = 0; i < nset_zmax.size(); ++i) {
      output << nset_zmax[i] + 1 << ", ";
      if ((i + 1) % 8 == 0)
        output << "\n";
    }
    output << "\n";

    // Elset
    std::vector<int> elset_count(256, 0);
    for (int i = 0; i < nc; ++i)
      elset_count[cells[i].value]++;

    std::vector<std::vector<int>> elsets(256);
    for (int i = 0; i < 256; ++i) {
      if (elset_count[i] > 0) {
        elsets[i].reserve(elset_count[i]);
      }
    }

    for (int i = 0; i < nc; ++i)
      elsets[cells[i].value].push_back(i);

    for (int i = 0; i < 256; ++i) {
      const std::vector<int> &elset = elsets[i];
      if (elset.size() > 0) {
        std::cout << "Elset_" << i << " (" << elset.size() << ")" << std::endl;
        output << "*elset, elset=elset_" << i << "\n";
        for (int k = 0; k < elset.size(); ++k) {
          output << elset[k] + 1 << ", ";
          if ((k + 1) % 8 == 0)
            output << "\n";
        }
        output << '\n';
      }
    }

    // std::vector<int> elset_yarn, elset_matrix;
    // elset_yarn.reserve(nc);
    // elset_matrix.reserve(nc);
    // for(int i=0; i< nc; ++i) {
    //    if (cells[i].value < 255) {
    //        elset_yarn.push_back(i);
    //    } else {
    //        elset_matrix.push_back(i);
    //    }
    //}
    // output << "*elset, elset=yarn\n";
    // for (int i=0; i<elset_yarn.size(); ++i) {
    //    output << elset_yarn[i] + 1 << ", ";
    //    if ((i+1)%8 == 0) output << "\n";
    //}
    // output << "\n";
    // output << "*elset, elset=matrix\n";
    // for (int i=0; i<elset_matrix.size(); ++i) {
    //    output << elset_matrix[i] + 1 << ", ";
    //    if ((i+1)%8 == 0) output << "\n";
    //}
    // output << "\n";

    // Orientation
    output << "*distribution table, name=ori-tab\n";
    output << "coord3d, coord3d\n";
    output << "*distribution, name=ori-dist, location=element, table=ori-tab\n";
    output << ", 1, 0, 0, 0, 1, 0\n";
    for (int i = 0; i < nc; ++i) {
      const int ii =
          static_cast<int>(filtered_vertices[cells[i].nodes[0]]->coords[0]);
      const int jj =
          static_cast<int>(filtered_vertices[cells[i].nodes[0]]->coords[1]);
      const int kk =
          static_cast<int>(filtered_vertices[cells[i].nodes[0]]->coords[2]);

      float u[3] = {dirx(ii, jj, kk), diry(ii, jj, kk), dirz(ii, jj, kk)};
      float v[3];

      // compute orthogonal vector
      int imax = argmax(u);
      /* if (imax==0) { */
      /*     v[0] = u[1]; */
      /*     v[1] = -u[0]; */
      /*     v[2] = 0; */
      /* } else if (imax==1) { */
      /*     v[0] = 0; */
      /*     v[1] = u[2]; */
      /*     v[2] = -u[1]; */
      /* } else { */
      /*     v[0] = -u[2]; */
      /*     v[1] =0; */
      /*     v[2] = u[0]; */
      /* } */

      if (imax == 0) {
        v[0] = 0;
        v[1] = 0;
        v[2] = 1;
      } else if (imax == 2) {
        v[0] = 1;
        v[1] = 0;
        v[2] = 0;
      } else {
        v[0] = 0;
        v[1] = 0;
        v[2] = 1;
      }

      // normalize v
      float nrm = std::sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
      v[0] /= nrm;
      v[1] /= nrm;
      v[2] /= nrm;

      output << i + 1 << ", " << static_cast<float>(u[0]) << ", "
             << static_cast<float>(u[1]) << ", " << static_cast<float>(u[2])
             << ", " << static_cast<float>(v[0]) << ", "
             << static_cast<float>(v[1]) << ", " << static_cast<float>(v[2])
             << "\n";
    }
    output << "*orientation, name=ori\n";
    output << "ori-dist\n";

  } else if (ext == "mesh") {
    // ------
    // Medit
    // ------
    output << "MeshVersionFormatted 1\n";
    output << "Dimension 3\n";
    output << "Vertices\n";
    output << nv << '\n';
    for (int i = 0; i < nv; ++i) {
      output << scale * filtered_vertices[i]->coords[0] << " "
             << scale * filtered_vertices[i]->coords[1] << " "
             << scale * filtered_vertices[i]->coords[2] << " 0\n";
    }
    output << "Hexahedra\n";
    output << nc << '\n';
    for (int i = 0; i < nc; ++i) {
      output << cells[i].nodes[0] + 1 << " " << cells[i].nodes[1] + 1 << " "
             << cells[i].nodes[2] + 1 << " " << cells[i].nodes[3] + 1 << " "
             << cells[i].nodes[4] + 1 << " " << cells[i].nodes[5] + 1 << " "
             << cells[i].nodes[6] + 1 << " " << cells[i].nodes[7] + 1 << " "
             << static_cast<int>(cells[i].value) << "\n";
    }
    output << "End";
  }

  output.close();

  return 0;
}
