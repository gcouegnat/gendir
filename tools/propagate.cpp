#include <argparser.h>
#include <image.h>
#include <utils.h>

int main(int argc, const char *argv[]) {
  Argparser parser;
  parser.add_option<std::string>("input", "");
  parser.add_option<int>("nx", "Dimension X", -1);
  parser.add_option<int>("ny", "Dimension Y", -1);
  parser.add_option<int>("nz", "Dimension Z", -1);
  parser.parse(argc, argv);

  const std::string input = parser.get<std::string>("input");

  int nx, ny, nz;
  if (!guess_size_from_filename(input, nx, ny, nz)) {
    nx = parser.get<int>("nx");
    ny = parser.get<int>("ny");
    nz = parser.get<int>("nz");
  }
  std::cout << nx << ", " << ny << ", " << nz << std::endl;

  Image<unsigned char> data(nx, ny, nz);
  data.load(input);

  for (unsigned int pass = 0; pass < 10; pass++) {

    std::cout << "Pass " << pass << std::endl;

    for (unsigned int i = 1; i < nx - 1; ++i) {
      for (unsigned int j = 1; j < ny - 1; ++j) {
        for (unsigned int k = 1; k < nz - 1; ++k) {

          if (data(i, j, k) == 32) {

            if ((data(i + 1, j, k) == 128) || (data(i, j + 1, k) == 128) ||
                (data(i, j, k + 1) == 128)) {
              data(i, j, k) = 128;
            } else if ((data(i + 1, j, k) == 255) ||
                       (data(i, j + 1, k) == 255) ||
                       (data(i, j, k + 1) == 255)) {
              data(i, j, k) = 255;
            }
          }
        }
      }
    }
  }

  data.dump("out.raw");
}
