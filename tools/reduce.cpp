#include <argparser.h>
#include <cmath>
#include <image.h>
#include <utils.h>

int main(int argc, const char *argv[]) {
  Argparser parser;
  parser.add_option<std::string>("input", "");
  parser.add_option<int>("nx", "", -1);
  parser.add_option<int>("ny", "", -1);
  parser.add_option<int>("nz", "", -1);
  parser.add_option<int>("factor", "", 2);
  parser.parse(argc, argv);

  const std::string filename = parser.get<std::string>("input");

  int nx, ny, nz;
  if (!guess_size_from_filename(filename, nx, ny, nz)) {
    nx = parser.get<int>("nx");
    ny = parser.get<int>("ny");
    nz = parser.get<int>("nz");
  }

  const int factor = parser.get<int>("factor");
  const int mx = nx / factor;
  const int my = ny / factor;
  const int mz = nz / factor;

  std::cout << nx << ", " << ny << ", " << nz << std::endl;
  std::cout << mx << ", " << my << ", " << mz << std::endl;

  Image<unsigned char> src(nx, ny, nz);
  src.load(filename);

  Image<unsigned char> dest(mx, my, mz);

#pragma omp parallel for
  for (int k = 0; k < mz; ++k) {
    for (int j = 0; j < my; ++j) {
      for (int i = 0; i < mx; ++i) {
        float acc = 0;
        for (int w = k * factor; w < std::min((k + 1) * factor, nz); ++w) {
          for (int v = j * factor; v < std::min((j + 1) * factor, ny); ++v) {
            for (int u = i * factor; u < std::min((i + 1) * factor, nx); ++u) {
              acc += static_cast<float>(src(u, v, w));
            }
          }
        }
        dest(i, j, k) =
            static_cast<unsigned char>(int(acc / (factor * factor * factor)));
      }
    }
  }

  std::string prefix, suffix;
  strip_filename(basename(filename, false), prefix, suffix);
  std::ostringstream oss;
  oss << prefix << mx << "x" << my << "x" << mz << suffix;

  dest.dump(oss.str());

  return 0;
}
