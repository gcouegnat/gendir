#include <argparser.h>
#include <direction.h>
#include <utils.h>

int main(int argc, const char *argv[]) {

    Argparser parser;
    parser.add_option<std::string>("basename", "");
    parser.add_option<int>("nx", "", -1);
    parser.add_option<int>("ny", "", -1);
    parser.add_option<int>("nz", "", -1);
    parser.add_option<std::string>("filter", "Filter: gaussian|box", "gaussian");
    parser.add_option<int>("ksize", "Window size", 5);
    parser.parse(argc, argv);

    const std::string basename = parser.get<std::string>("basename");
    int nx, ny, nz;
    if (!guess_size_from_filename(basename, nx, ny, nz)) {
        nx = parser.get<int>("nx");
        ny = parser.get<int>("ny");
        nz = parser.get<int>("nz");
    }
    std::cout << nx << ", " << ny << ", " << nz << std::endl;

    const std::string filtertype = parser.get<std::string>("filter");
    const int ksize = parser.get<int>("ksize");

    std::cout << "Using " << filtertype << " filter of size " << ksize
        << std::endl;

    std::vector<float> filter;
    if (filtertype == "gaussian") {
        filter = gaussian_filter_sep<float>(ksize);
    } else if (filtertype == "box") {
        filter = box_filter_sep<float>(ksize);
    }

    Image<float> gradx(nx, ny, nz);
    Image<float> grady(nx, ny, nz);
    Image<float> gradz(nx, ny, nz);

    gradx.load(basename + "_gradx.raw");
    grady.load(basename + "_grady.raw");
    gradz.load(basename + "_gradz.raw");

    Image<float> dirx(nx, ny, nz);
    Image<float> diry(nx, ny, nz);
    Image<float> dirz(nx, ny, nz);
    /* Image<float> norx(nx, ny, nz); */
    /* Image<float> nory(nx, ny, nz); */
    /* Image<float> norz(nx, ny, nz); */
    Image<float> anisotropy(nx, ny, nz);
    Image<float> energy(nx, ny, nz);


    std::cout << "Using " << num_threads() << " thread(s)" << std::endl;

    // compute_directions(gradx,grady,gradz,dirx,diry,dirz,filter);
    compute_directions_sep(
            gradx, grady, gradz, 
            dirx, diry, dirz,
            /* norx, nory, norz, */
            filter, anisotropy, energy);

    dirx.dump(basename + "_dirx.raw");
    diry.dump(basename + "_diry.raw");
    dirz.dump(basename + "_dirz.raw");
    /* norx.dump(basename + "_norx.raw"); */
    /* nory.dump(basename + "_nory.raw"); */
    /* norz.dump(basename + "_norz.raw"); */
    anisotropy.dump(basename + "_ani.raw");
    energy.dump(basename + "_nrj.raw");

    return 0;
}
