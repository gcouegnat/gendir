# Gendir

This code computes local direction in 3D images using the structure tensor approach (https://en.wikipedia.org/wiki/Structure_tensor).

## Installation

```
mkdir -pv build
cd build
cmake ..
make
```

## Usage

**1. Gradient Computation**

The first step involves calculating the image gradients using the `gradient` tool. It employs an optimized kernel designed for analyzing fiber materials ([10.1007/s11760-007-0035-2](https://doi.org/10.1007/s11760-007-0035-2)]).

```
USAGE:  ./gradient --input <string> [options]

OPTIONS:
--input <string>     Input filename
--nx <int>     Size in X direction (default value: -1)
--ny <int>     Size in Y direction (default value: -1)
--nz <int>     Size in Z direction (default value: -1)
```


- The input is expected to be a 3D image represented as an 8-bit unsigned char array.
- The filename can optionally specify image dimensions (e.g., `input_500x450x450.raw`). If not provided, use the `--nx`, `--ny`, `--nz` flags to specify dimensions manually.
- The tool generates three output files, one for each gradient direction (X, Y, Z). These files use the same base name as the input with `_gradx.raw`, `_grady.raw`, `_gradz.raw` suffixes, respectively.
- The output files are in 32-bit floating-point format.

**2. Local Direction Calculation**

The second step uses the `direction` tool to compute the local direction based on the previously calculated gradients.

```
USAGE:  ./direction --basename <string> [options]

OPTIONS:
--basename <string>  Base name of gradient files (without extension)
--nx <int>      Size in X direction (default value: -1)
--ny <int>      Size in Y direction (default value: -1)
--nz <int>      Size in Z direction (default value: -1)
--ksize <int>     Window size for smoothing kernel (default value: 5)
```


- The `basename` argument specifies the base filename of the gradient files (without the `.raw` extension). For example, for `input_500x450x450_gradx.raw`, the base name would be `input_500x450x450`.
- Similar to the `gradient` tool, image dimensions can be provided with the `--nx`, `--ny`, `--nz` flags or automatically retrieved from the filename format.
- The tool generates three output files, each representing a component of the normalized local direction vector (X, Y, Z). These files use suffixes like `_dirx.raw`, `_diry.raw`, `_dirz.raw` appended to the base name.
- The standard deviation ($\sigma$) of the Gaussian kernel is calculated based on the `ksize` parameter using the formula:  `sigma = ((ksize - 1) * 0.5 - 1) * 0.3 + 0.8`.
